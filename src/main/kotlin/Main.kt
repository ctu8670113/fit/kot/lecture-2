package cz.ctu.fit.bi.kot.heap

import cz.ctu.fit.bi.kot.heap.PriorityQueue.Companion.heapSort


fun main() {
    val pq = PriorityQueue(1.0, 33.0)

    println(pq)
    pq.add(46.0)
    println(pq)
    pq.add(6.0)
    println(pq)
    pq.add(4.0)
    println(pq)
    pq.add(16.0)
    println(pq)
    pq.add(36.0)
    println(pq)
    println(36.0 in pq)
    while (!pq.isEmpty())
        println(pq.remove())
    val array = arrayOf(4.0, 10.0, 3.0, 5.0, 1.0, 2.0, 6.0, 8.0, 7.0, 9.0)
    array.heapSort()
    println("[ ${array.joinToString(", ")} ]")
}